﻿-- listar equipos (Todos los campos) ciclistas que hayan ganado alguna etapa con puerto de montañSELECT DISTINCT
  equipo.*
FROM equipo
  JOIN ciclista
    ON equipo.nomequipo = ciclista.nomequipo;
-- JOIN etapa ON ciclista.dorsal=etapa.dorsal
-- JOIN puerto ON etapa.numetapa=puerto.numetapa;


-- sacamos primero las etapas que tienen puerto c1
        
        SELECT DISTINCT
          e.numetapa,
          e.dorsal
        FROM etapa e
          JOIN puerto p
            ON e.numetapa = p.numetapa;

  -- sacamos los ciclistas que hayan ganado esas c2    

            SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (
              
                SELECT distinct e.numetapa, e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa--
              ) AS c1 ON c.dorsal=c1.dorsal;

   -- consulta final
    SELECT e.* FROM equipo e JOIN(
    
    SELECT DISTINCT c.nomequipo FROM ciclista c JOIN (
      
        SELECT distinct e.numetapa, e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa--
      ) AS c1 ON c.dorsal=c1.dorsal
    ) AS c2 ON c2.nomequipo=e.nomequipo;


    CREATE OR REPLACE VIEW consulta1C1 AS 
      SELECT DISTINCT e.numetapa,  e.dorsal
      FROM etapa e
      JOIN puerto p  ON e.numetapa = p.numetapa;

    DROP VIEW consulta1C1;


    SELECT * FROM consulta1C1;

    CREATE OR REPLACE VIEW consulta1C2 AS 
      
            SELECT DISTINCT c.nomequipo
              FROM ciclista c
              JOIN consulta1C1 c1            
                
            
    ON c.dorsal=c1.dorsal;

-- equipos que tengan ciclistas que hayan ganado puerto de una altura mayor de 1200

      SELECT c.* FROM ciclista c JOIN puerto p ON c.dorsal=p.dorsal WHERE p.altura>1200;

 -- mejorar las consultas mediante subconsultas

 -- c1 

  SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;

  -- consulta completa


    SELECT c.* FROM (
        SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200
      ) AS c1
      JOIN ciclista c ON c.dorsal=c1.dorsal;
-- vamos a utilizar vistas

  CREATE OR REPLACE VIEW consulta2c1 as

  SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura>1200;

  -- creamos la segunda vista
    CREATE OR REPLACE VIEW consulta2 AS
      SELECT c.* FROM consulta2c1 c1
      JOIN ciclista c USING(dorsal);

-- consulta 3 los ciclistas que no han ganado etapas

  SELECT
  c.* 
  FROM ciclista c
  LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE e.dorsal IS NULL;



  -- realizado con subconsultas

 -- c1
    SELECT DISTINCT dorsal FROM etapa;

    -- consulta completa

      SELECT c.* FROM ciclista c LEFT JOIN (
         SELECT DISTINCT dorsal FROM etapa
        ) AS c1
        ON c.dorsal = c1.dorsal WHERE c1.dorsal IS NULL;
      -- la resta pero con "not in"

        SELECT * FROM ciclista WHERE dorsal NOT IN (
              SELECT DISTINCT dorsal FROM etapa);
-- realizar con vistas
-- c1
CREATE OR REPLACE VIEW consulta3c1 AS  SELECT DISTINCT dorsal FROM etapa;

-- completa
  CREATE OR REPLACE VIEW consulta3 as

  SELECT
    c.*
  FROM ciclista c LEFT JOIN consulta3c1 c1
  ON c.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

  -- completa con not in










