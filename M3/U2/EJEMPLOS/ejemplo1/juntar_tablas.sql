﻿
-- inner join con on

SELECT * FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal;


-- natural join

SELECT * FROM ciclista NATURAL JOIN puerto;

-- inner join con using

SELECT * FROM ciclista INNER JOIN puerto USING (dorsal);

-- inner join con where= =

SELECT * FROM ciclista INNER JOIN puerto WHERE ciclista.dorsal=puerto.dorsal;

-- producto cartesiano

  SELECT * FROM ciclista, puerto WHERE ciclista.dorsal=puerto.dorsal;

-- juntar 3 tablas
-- opcion 1

  SELECT * FROM equipo JOIN ciclista JOIN etapa ON ciclista.nomequipo=etapa.nomequipo AND c.dorsal=e.dorsal;
-- opcion 2

  SELECT * FROM equipo JOIN ciclista ON e.nomequipo=c.nomequipo JOIN etapa ON c.dorsal=e.dorsal;


 -- opcion 3
  SELECT * FROM equipo JOIN ciclista USING(nomequipo)
                       JOIN etapa USING(dorsal);


  
  




