﻿-- ciclistas que hayan ganado alguna etapa y que no hayan ganado ningun puerto

  -- 1.- realizarlo sin vistas
  -- 2.- con las vistas necesarias

-- c1 ciclistas que han ganado alguna etapa

  SELECT distinct dorsal FROM etapa;

  -- c2 ciclistas que no han ganado ningun puerto;

    SELECT * FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL;

    -- sin utilizar vistas
-- c1 : ciclistas que han ganado etapas
      SELECT DISTINCT e.dorsal FROM etapa e;
-- c2 : ciclistas que han ganado puerto
  SELECT DISTINCT p.dorsal FROM etapa e;
-- c3: ciclistas que no han ganado puertos
  SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
    WHERE p.dorsal IS NULL;

      -- utilizando vistas

        CREATE OR REPLACE VIEW consulta4c1 AS 
    SELECT DISTINCT e.dorsal FROM etapa e;

        CREATE OR REPLACE VIEW consulta4c2 AS
  SELECT DISTINCT p.dorsal FROM etapa e;

        CREATE OR REPLACE VIEW  consulta4c3 AS
          SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
    WHERE p.dorsal IS NULL;

        -- c1-c3 : ciclistas que han ganado etapas y puertos

          SELECT c1.dorsal FROM consulta4c1 c1 LEFT JOIN consulta4c3 c3 USING(dorsal)
            WHERE c3.dorsal IS NULL;

          SELECT * FROM consulta4c1 c1 WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c3);

            -- c1-c2 : ciclistas que han ganado etapas y no han ganado puertos


              SELECT c1.dorsal FROM consulta4c1 c1 LEFT JOIN consulta4c2 USING(dorsal) WHERE c2.dorsal IS NULL;

              SELECT * FROM consulta4c1 c1 WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c2);

              -- c1 UNION c2  ciclistas que han ganado etapas mas los ciclistas que han ganado puertos

SELECT * FROM consulta4c1 UNION SELECT * FROM consulta4c2;

-- c1 union c3: ciclistas que han ganado etapas mas los ciclistas que no han ganado puertos

  SELECT * FROM consulta4c1 c
    UNION
  SELECT * FROM consulta4c3 c;

  -- c1 interseccion c3 : ciclistas que han ganado etapas y que no han ganado puertos

SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c3 c3;

-- c1 interseccion c2: ciclistas que han ganado etapas y ademas han ganado puertos
  SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c2 c2;


  -- c3-c1 , c3interc2
-- c3-c2 , c3interc1
    -- c4-c3-c2
    -- c1 u c3
    -- c1 u c3 u c4

   -- c1 proyeccion dorsal(lleva)
   -- c2 ciclista - c1
   -- c3 proyeccion dorsal (etapa)
  --  c4 proyeccion (puerto)


-- c1 los ciclistas que han llevado maillot
    SELECT distinct dorsal FROM lleva;

  CREATE OR REPLACE VIEW consulta5c1 AS
  SELECT distinct dorsal FROM lleva l;
-- c2: los ciclistas que no han llevado maillot
  SELECT c.dorsal FROM ciclista c LEFT JOIN consulta5c1 c1 USING (dorsal) WHERE c1.dorsal IS NULL;

  SELECT * FROM ciclista c
    WHERE c.dorsal NOT IN (SELECT * FROM consulta5c1);

  CREATE OR REPLACE VIEW consulta5c2 AS
SELECT c.dorsal FROM ciclista c LEFT JOIN consulta5c1 c1 USING (dorsal) WHERE c1.dorsal IS NULL;

  -- c3: ciclistas que han ganado etapas;
    SELECT DISTINCT dorsal FROM etapa;

    CREATE OR REPLACE VIEW consulta5c3 AS 
      SELECT DISTINCT dorsal FROM etapa;

    -- c4: ciclistas que han ganado puertos
      SELECT DISTINCT dorsal FROM puerto;
      CREATE OR REPLACE VIEW consulta5c4 AS
        SELECT DISTINCT dorsal FROM puerto;

      -- c3-c1 : ciclistas que han ganado etapas y ademas que no han llevado maillot
        SELECT * FROM consulta5c3 c3 LEFT JOIN consulta5c1 c1 USING(dorsal)
          WHERE c1.dorsal IS NULL;

     select *  from consulta5c3 c3
      WHERE c3.dorsal NOT IN (SELECT * FROM consulta5c1);

  -- c4-c3-c2  ciclistas que han ganado puertos y que no han ganado etapas y que han llevado maillot
     SELECT c4.dorsal FROM consulta5c4 c4 LEFT JOIN consulta5c3 c3 USING(dorsal)
                                 LEFT JOIN consulta5c2 c2 USING(dorsal)
                                 WHERE
                                    c2.dorsal IS NULL AND c3.dorsal IS NULL;
-- c1 union c3 : ciclistas que han llevado maillot mas los que han ganado etapas
  SELECT * FROM consulta5c1 c1 UNION SELECT * from consulta5c3 c3;


-- c1 union c3 union c4
  SELECT * FROM consulta5c1 c1
    UNION
  SELECT * FROM consulta5c2 c2
    UNION
  SELECT * FROM consulta5c3 c3; 







