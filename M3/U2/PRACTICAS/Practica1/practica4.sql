﻿-- 1.Nombre y edad de los ciclistas que han ganado etapas

  -- c1 : ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- consulta completa con join
    SELECT
      c.nombre,c.edad
      FROM(SELECT DISTINCT e.dorsal FROM etapa e) c1
      JOIN ciclista c USING(dorsal);

   -- correcta pero sin obtimizar
    SELECT DISTINCT c.nombre,c.edad FROM etapa e JOIN ciclista c ON e.dorsal = c.dorsal;

    -- consulta completa con in

      SELECT c.nombre,c.edad
        FROM ciclista c
       WHERE  c.dorsal IN (SELECT DISTINCT e.dorsal FROM etapa e);

      -- creando vistas
        CREATE OR REPLACE VIEW ganadores_de_etapa AS 
        SELECT DISTINCT e.dorsal FROM etapa e;

        SELECT
          c.nombre,c.edad
          FROM ganadores_de_etapa
          JOIN ciclista USING(dorsal);

          DROP VIEW ganadores_de_etapa;     



-- 2.Nombre y edad de los ciclistas que han ganado puertos

-- optimizar
-- c1
  SELECT DISTINCT p.dorsal FROM puerto p;

  SELECT c.nombre,c.edad
  FROM ciclistas c 
  JOIN(SELECT DISTINCT p.dorsal FROM puerto p) AS c1
  USING (dorsal);

-- utilizar vistas
  CREATE OR REPLACE VIEW c1 AS
      SELECT DISTINCT p.dorsal FROM puerto p;

  SELECT
    c.nombre,c.edad
    FROM
    ciclista c
    JOIN c1
    USING(dorsal)

    DROP VIEW c1;


  -- 3 Nombre y edad de los ciclistas que han ganado etapas y puertos

    SELECT DISTINCT c.nombre,c.edad FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal
                           JOIN puerto p ON c.dorsal = p.dorsal;
-- consulta optimizada 
CREATE OR REPLACE VIEW c1 AS
  SELECT DISTINCT e.dorsal FROM etapa e;

CREATE OR REPLACE VIEW c2 AS
  SELECT * FROM c1 NATURAL JOIN c2;



-- consulta optimizada sin vistas
-- c3
  

  SELECT * FROM (
    SELECT DISTINCT e.dorsal FROM etapa e) c1
    NATURAL JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c2;
-- 4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

  SELECT e.director FROM equipo e WHERE e.nomequipo
  IN (SELECT DISTINCT c.nomequipo FROM ciclista c JOIN(SELECT DISTINCT dorsal FROM etapa) e1 ON e1.dorsal=c.dorsal);
   

  -- consulta optimizada

--  dorsal y nombre de los ciclistas que hayan llevado algún maillot

   -- c1
    SELECT DISTINCT dorsal FROM lleva;

    SELECT * FROM ciclista JOIN (    SELECT DISTINCT dorsal FROM lleva) USING (dorsal);
-- 6.dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

  -- c1

  SELECT DISTINCT dorsal,nombre FROM ciclista JOIN lleva USING(dorsal)JOIN maillot USING(código) WHERE color='amarillo';

-- c1 :código del maillot amarillo
    CREATE OR REPLACE VIEW C1 as
  SELECT DISTINCT código FROM maillot WHERE color='amarillo';

 -- c2 : dorsal de los ciclistas que han llevado maillot amarillo
  CREATE OR REPLACE VIEW C2 as
  
  SELECT distinct dorsal FROM lleva WHERE código=( SELECT DISTINCT código FROM maillot WHERE color='amarillo'); 

-- c2p 
  SELECT * FROM lleva JOIN C1 USING(código);

-- resultado

  SELECT nombre,dorsal FROM ciclista JOIN c2 USING(dorsal);

  -- 7 dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas

-- c1 : ciclistas que han llevado maillot
    CREATE OR REPLACE VIEW C1 as
    SELECT distinct dorsal FROM lleva;
-- c2 : ciclistas que han ganado etapas
    CREATE OR REPLACE VIEW C2 as
    SELECT DISTINCT dorsal FROM etapa;

 -- resultado
SELECT * FROM c1 NATURAL JOIN c2;

-- 8 indicar el numetapa de las etapas que tengan puertos


SELECT DISTINCT numetapa FROM puerto;

-- 9 indicar los km de las etapas que hayan ganado ciclistas de Banesto y que tengan puerto

-- c1 : etapas que tienen puerto
  SELECT DISTINCT numetapa FROM puerto;

-- c2 : ciclistas de Banesto
  SELECT dorsal FROM ciclista WHERE nomequipo='Banesto',

-- c3 : etapas que han ganado ciclistas de Banesto
  SELECT * FROM etapa JOIN c2 USING(dorsal);

-- c4 : etapas que tienen puerto y que han ganado ciclistas de Banesto
  SELECT * FROM c3 NATURAL JOIN c1;
-- resultado
  SELECT kms from c4 JOIN etapa USING(numetapa);

  -- 10 listar el numero de ciclistas que han ganado etapa con puerto
-- 1 : ciclistas que han ganado etapas con 
    CREATE OR REPLACE VIEW c1 as
    SELECT DISTINCT dorsal FROM puerto JOIN etapa USING(numetapa);
 
SELECT COUNT(*) AS nciclistas FROM c1;

-- intentar realizarlo en una sola
  SELECT COUNT(DISTINCT etapa.dorsal) AS nciclistas FROM puerto JOIN etapa USING(numetapa);

-- 11 nombre de los puertos que hayan sido ganados por ciclistas de Banesto

  -- c1 
  CREATE OR REPLACE VIEW c1 as

  SELECT dorsal FROM ciclista WHERE nomequipo='Banesto';
  SELECT * FROM puerto JOIN c1 USING(dorsal);

  -- 12 numero de etapas que tengan puertos ganados por ciclistas de Banesto de mas de 200 km
-- c1 ciclistas de Banesto
   SELECT * FROM ciclista WHERE nomequipo='Banesto';

  -- c2 
  SELECT COUNT(DISTINCT etapa.numetapa) FROM c1 JOIN etapa USING(dorsal) JOIN puerto USING(numetapa) WHERE kms>20;






   
