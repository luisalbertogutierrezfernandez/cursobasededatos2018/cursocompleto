﻿-- (22) Seleccionar los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.

  SELECT * FROM emple WHERE oficio='vendedor' ORDER BY apellido;

 -- (24) Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente
  SELECT * FROM emple WHERE dept_no=10 AND oficio='analista' ORDER BY apellido,oficio;
  -- (25) Realizar un listado de los distintos meses en que los empleados se han dado de alta

    SELECT DISTINCT MONTH(fecha_alt) FROM emple;
-- (26)-- Realizar un listado de los distintos años en los que los empleados se han dado de alta

SELECT DISTINCT year (fecha_alt) AS YEAR FROM emple;

-- 27) Realizar un listado de los distintos días del mes en que los empleados se han dado de alta

  SELECT DISTINCT DAY (fecha_alt) AS dia FROM emple;

-- (28) Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20

  SELECT apellido FROM emple WHERE salario>2000 OR dept_no=20;

-- (35) Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados

  SELECT apellido FROM emple WHERE apellido LIKE 'A%' OR apellido LIKE 'M%';

-- (37) Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición.
 --  Ordenar la salida por oficio y por salario de forma descendente

SELECT * FROM emple WHERE apellido LIKE 'A%' AND oficio LIKE '%E%' ORDER BY oficio, salario DESC;