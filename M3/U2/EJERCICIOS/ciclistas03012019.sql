﻿/*Consultas de seleccion 2*/

  -- numero de ciclistas que hay

  SELECT COUNT(*) FROM ciclista;

  -- ciclistas de Banesto;

    SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto';

    -- la edad media de los ciclistas

      SELECT AVG(edad) FROM ciclista;
--la edad media de los de Banesto

      SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto';

-- la edad media de los ciclistas por cada equipo

  SELECT AVG(edad) FROM ciclista GROUP BY nomequipo;

  --numero de ciclistas por equipo

  SELECT COUNT(*),nomequipo FROM ciclista GROUP BY nomequipo;

  -- total de puertos

    SELECT COUNT(nompuerto) FROM puerto;

-- puertos mayores de 1500

  SELECT COUNT(nompuerto) FROM puerto WHERE altura>1500;

  -- equipos de mas de 4 ciclistas

 SELECT count(*) AS total ,nomequipo FROM ciclista GROUP BY nomequipo HAVING total>4;

-- equipos de mas de 4 ciclistas cuya edad entre 28 y 32

 SELECT COUNT(*) AS total ,nomequipo FROM ciclista WHERE edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING total>4;


-- indicame el n. de etapas ganadas por cada uno de ciclistas

SELECT COUNT(*), dorsal FROM etapa GROUP BY dorsal;
-- indicame el dorsal de los ciclistas que han ganado mas de 1 etapa;

