﻿/* Consultas de combinacion externas*/

  -- Ejer. 5 dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca
 


    -- los ciclistas que han llevado el amarillo
      SELECT DISTINCT lleva.dorsal FROM lleva JOIN maillot USING (código) WHERE color='amarillo';

    -- resultado
      SELECT * FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM lleva JOIN maillot USING (código) WHERE color='amarillo') c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;

   -- Ejer. 6 indicar el numero de las etapas que no tengan puertos.

    -- etapas con puertos
    SELECT DISTINCT numetapa FROM puerto;
    -- resultado
    SELECT * FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto) c1 ON  etapa.numetapa=c1.numetapa WHERE c1.numetapa IS NULL;

    -- Ejer. 7 indicar la distancia media de las etapas que no tienen 
      SELECT AVG(kms) FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto) c1 ON  etapa.numetapa=c1.numetapa WHERE c1.numetapa IS NULL;

    --  Ejer. 8 listar el numero de ciclistas que no hayan ganado alguna etapa;

      -- ciclistas que han ganado alguna etapa
      SELECT distinct dorsal FROM etapa;
      SELECT * FROM ciclista LEFT JOIN (SELECT DISTINCT dorsal FROM etapa) c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;
   -- Ejer. 9 listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto

    -- c1
    SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;

    -- Ejer. 10 listar dorsal de los ciclistas que hayan ganado unicamente etapas que no tengan puerto
      -- ciclistas que han ganado etapas con puerto c2
      SELECT distinct dorsal FROM puerto;
    -- c1 - c2

      SELECT * FROM  ( SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL) 
      LEFT JOIN (SELECT distinct dorsal FROM puerto) ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;
     


